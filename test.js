const assert = require('assert');
const math = require('./math');
describe('file to be tested', () => {
    context('function to be tested', () => {
        it('should do something', () => {
            assert.equal(1 , 1);
        });
        it('should do another thing', () => {
            assert.deepEqual([1,2,3], [1,2,3])
            //Array in JavaScript is an 'object' if use .equal test won't pass 
            assert.deepEqual({name: 'kob'}, {name: 'kob'});
            //If want to test an object as object must use deepEqual
        });
    });
});

describe('file math', () => {
    context('function add1', () => {
        it('should do add(1,2)', () => {
            assert.equal(math.add1(1,2), 3);
        });
    });
    context('function add2', () => {
        it('should add2(5,5)', () => {
            assert.equal(math.add2(5,5), 10);
        });
    });
});

// const math = require('./math');
// const add = math.add1;
//console.log(math.add1(1,1));
// console.log(add(1,1));